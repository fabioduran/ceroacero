#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import re


class liga_teams():
    """
    Clase que permite extraer información relativa a los equipos.
    """

    def get_teams(self, soup):
        """
        Método que permite extraer los ID de los equipos que participan en una
        temporada de la liga en particular.

        Recibe como parámetro:
            * soup: objeto de tipo soup

        Retorna:
            * list_team: lista con todos los id de los equipos de la temporada
        """
        list_team = []
        team = soup.find('div', {'id': 'edition_table'})
        team = team.find('tbody')
        team = team.find_all('tr')

        for i in team:
            team_id = i.find_all('a')

            team_id = team_id[2]['href']

            team_id = ''.join(re.findall(r'equipa=(\d+)', team_id))
            list_team.append(team_id)

        return list_team
