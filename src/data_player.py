#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class data_player():
    """
        Clase que extrae información relativa a los jugadores desde la web
        ceroacero.es
    """

    def write_csv(self, dataset):
        """
        Escribe en un fichero de texto en formato csv a partir de una lista de
        jugadores.

        Recibe:
            dataset: lista de datos de cada uno de los jugadores.
            Los datos deben en orden: equipo, temporada, id del jugador,
            nombre del jugador, posicion, nacionalidad, edad.
        """

        # if os.path.exists("player_data.csv"):
        f = open("player_data.csv", "a")
        row = ''
        for i in dataset:
            row = row + ';'.join(i)
            row = row + "\n"
        f.write(row)
        f.close()

    def get_team(self, soup):
        """
        Método que agrega valor al atributo team el valor del equipo extraído
        a partir de un objeto de tipo bs4.BeautifulSoup

        Recibe soup: objeto de tipo bs4.BeautifulSoup
        """
        tmp = soup.find_all('span', {'class': 'name'})
        self.team = tmp[-1].text

    def get_season(self, soup):
        """
        Método que agrega valor al atributo season el valor de la temporada
        a partir de un objeto de tipo soup
        """
        tmp = soup.find('h2', {'class': 'header'}).text
        tmp = [s for s in tmp if s.isdigit() or s == '/']
        self.season = ''.join(str(i) for i in tmp)
        # print(self.season)

    def get_team_squad(self, soup):
        """
        Método que extrae información relativa a los jugadores

        Recibe:
            soup: objeto de tipo bs4.BeautifulSoup

        Retorna:
            Objeto de tipo lista que llama al método de escritura de csv
        """


        dataset = []
        team = soup.find('div', {'id': 'team_squad'})
        innerbox = team.find_all('div', {'class': 'innerbox'})
        for i in innerbox:
            position = i.find('div', {'class': 'title'}).text
            print(position)
            # all_player = i.find_all('div', {'class': 'staff_line'})
            all_player = i.find_all('div', {'class': 'staff'})
            for x in all_player:
                number = x.find('div', {'class': 'number'}).text
                print('Player Number:', number)
                tmp = x.find_all('a')
                # Country
                country = tmp[0].find('img').get('title')
                print('Country:', country)
                _id = tmp[1]['href']
                _id = _id.split('?id=')[1]
                # ID
                print('ID: ', _id)
                # nombre del jugador
                name = tmp[1].text
                print('Name:', name)
                print('Position:', position)
                # age
                age = x.find('span').text
                print('Age:', age)
                # Season
                print('Season:', self.season)
                print('Team:', self.team)
                print('**************')
                dataset.append([self.team,
                                self.season,
                                _id,
                                name,
                                number,
                                position,
                                country,
                                age])
        self.write_csv(dataset)
