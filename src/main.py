#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from scraping import scraping
from data_player import data_player
from liga_teams import liga_teams
import threading
import time
import logging


def get_teams():
    # Teams
    s = scraping()
    soup = s.put_url("https://www.ceroacero.es/edicao.php?id_edicao=135635")
    liga = liga_teams()
    teams = liga.get_teams(soup)
    return teams


def start_scraping(team=None, epoch=None):
    s = scraping()
    # Players
    # soup = s.put_url("http://localhost/~fabio/ceroacero/archivo.html")
    url = "https://www.ceroacero.es/equipa.php?id={team}".format(team=team)
    url = url + "&epoca_id={epoca}".format(epoca=epoch)
    soup = s.put_url(url)
    data = data_player()
    data.get_team(soup)
    data.get_season(soup)
    data.get_team_squad(soup)


def start():

    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
    threads = list()
    teams = get_teams()
    count = 0
    epoch_start = 100
    epoch_finish = 149 + 1
    for epoch in range(epoch_start, epoch_finish):
        for i in range(len(teams)):
            try:
                team = teams[i]
                x = threading.Thread(target=start_scraping(team, epoch), args=(i,))
                threads.append(x)
                x.start()
            except AttributeError:
                print("Equipo no existe para esa temporada ")

            count = count + 1
            if count == 5:
                for thread in threads:
                    thread.join()
                threads = list()
                time.sleep(400)
                count = 0


if __name__ == "__main__":
    start()
