#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
import random

class scraping():

    def __init__(self):
        pass

    def get_user_agent(self):
        """
            Función que retorna un tipo de agente mediante un random
        """

        agent = ["Mozilla/5.0 (X11; Linux x86_64; rv:70.0) Gecko/20100101 \
                 Firefox/70.0",
                 "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) \
                 Gecko/20100101 Firefox/52.0",
                 "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/605.1.15 \
                 (KHTML, like Gecko) Version/13.0 Safari/605.1.15 \
                 Epiphany/605.1.15",
                 "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 \
                 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36",
                 ]

        user_agent = random.choice(agent)
        return user_agent


    def get_headers(self):
        """
            Función que retorna un diccionario con los headers que usará
            la aplicación para el request html
        """
        headers = {
                    "Accept": "text/html,application/xhtml+xml,application/xml;\
                    q=0.9,image/webp,*/*;q=0.8",
                    "Accept-Encoding": "gzip, deflate, sdch, br",
                    "Accept-Language": "en-US,en;q=0.8",
                    "Cache-Control": "no-cache",
                    "dnt": "1",
                    "Pragma": "no-cache",
                    "Upgrade-Insecure-Requests": "1",
                    "User-Agent": self.get_user_agent(),
        }
        return headers

    def put_url(self, url):
        """
            Función que instancia la conexión a una web en particular

            Recibe:
                url: String con la dirección web a extraer

            Retorna:
                soup: Objeto del tipo BeautifulSoup
        """
        response = requests.get(url, self.get_headers())
        # response = requests.get("http://www.ceroacero.es/player.php?id=1",
                        # headers=headers)
        soup =  BeautifulSoup(response.text, 'html.parser')
        return soup
