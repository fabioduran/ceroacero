# ceroacero

El objetivo de esta actividad será la creación de un dataset a partir de los datos contenidos en una web, para nuestro caso se trata de ceroacero.es

# Equipo
Fabio Durán Verdugo y Daniel Oriol Asensio. 

# Requerimientos e instalación:
El trabajo se ha realizado utilizando Python3, se recomienda el uso de Python3 en una versión mayor a 3.5.2

# Creación de un entorno virtual para la ejecución de la aplicación
Para crear un entorno virtual o un sandbox propio para la aplicación se puede utilizar el módulo python3-env e instalar del siguiente modo:

### Debian y Ubuntu
```bash
apt install python3-venv
````

### Versiones basadas en RPM
```bash
yum install python3-venv
```
or
```bash
dnf install python3-venv
```

Luego de instalar el paquete es necesario crear el entorno virtual con el siguiente comando
```bash
$ python3 -mvenv FOLDER
```
En donde FOLDER es el nombre del directorio en donde se encontrará nuestro entorno virtual.

## Activar el Entorno Virtual (opcional)
Para activar el entorno virtual debemos instanciar dicha ejecución:
```bash
$ cd FOLDER
$ source bin/activate
```

Esto cambiará las rutas de python (PYTHONPATH) para que sean instanciadas desde la ruta de nuestro entorno virtual.

# Instalación de los requerimientos:
Para instalar los requerimientos se puede realizar utilizando pip, para ello se debe utilizar el archivo requirements.txt incluído en el repositorio:

```bash
$ pip install wheel
$ pip install -r requirements.txt
```

# Ejecución de la aplicación
Todo el source se encuentra en el directorio src del proyecto, y el script a ejecutar es main.py
```bash
$ python main.py
``` 

Ahora esperar que termine la recogida de los datos que serán almacenados en un archivo csv llamado player_data.csv


# Licencia del DataSet
Licencia seleccionada: Licencia Reconocimiento-NoComercial 4.0 Internacional

* Se permite que se compartan las adaptaciones de esta obra.
* No se permiten usos comerciales de esta obra (la data ha sido recolectada por terceros).

